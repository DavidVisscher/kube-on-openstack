---

kanban-plugin: basic

---

## To Do

- [ ] Configure #bastion and move #salt master to private net. <br>Results from decisions on [[2022-05-02]]. ^awk378


## In Progress

- [ ] Deploy machines using #terraform


## Done

**Complete**
- [x] Look up reference implementations for kubernetes




%% kanban:settings
```
{"kanban-plugin":"basic","show-checkboxes":true,"new-note-folder":"notes","link-date-to-daily-note":true}
```
%%
Spent the day mostly on other subjects and meetings.


# On the rationale of #Gitlab vs #Github 
contd. from [[2022-05-02#Basic terraform setup]]
I discussed the matter with Alexander over Slack.
Alexander indicated a preference for github, but also that deviating from it is possible, quote:
 > we can also switch to gitlab if that is important. My rationale for github was that (i) it is something I personally know better, (ii) it is as standard de-facto for all open source projects
 > for the rest - looks good, looking forward for first results ;-)))

I agree with this rationale. However, when looking at github, I run into the following issues:

The devops specific features that GitLab has, but GitHub doesn't, can be replaced by other cloud services. Those services are often expensive however, running against the goal of openness.
Just terraform cloud alone starts at $20 per user per month.

What I'll be needing from it is :
- CI/CD features (rolling stuff out automatically)
- GitLab environments, to monitor the kube and collect metrics on the applications
- Manage terraform state files
- (maybe) use the container registry

In the end, using an open GitLab project may be easier and more accessible than GitHub+additional services.

I need to make sure this reasoning is well documented and clear for the end-project.
# Meeting with Alexander
Meeting with Alexander fell through, should probably reschedule.
- [x] Reschedule meeting with Alexander when he gets better 📅 2022-05-09 ✅ 2022-05-23

# Meeting with Mostafa
Instead, I'm meeting with Mostafa at 13:00 to discuss ECiDA as part of the context of the problem.
- [x] Meet with Mostafa to understand ECiDA better
I had a very nice meeting with Mostafa, and he walked me trough the requirements that ecida would have regarding the environment.
![[2022_05_04 13_49 whiteboard.jpg]]
Some things to keep in mind for the future:
	- Cluster autoscaling would be nice. If possible, try to connect the kube cluster to openstack to facilitate this. Upscaling is probably easier than downscaling
	- Mostafa had the idea to sacrifice dev cluster nodes in favour of those in production when resources are needed. This seems complex to implement, but there may be a foundation I can lay for it.
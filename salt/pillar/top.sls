#!py


def add_if_pillar_file_exists(in_list: list, filename: str, strip_sls=True):
    """
    Adds a file to the list if it exists in the pillar, otherwise do nothing.
    Operates on the list in-place
    :param in_list: list to add to
    :param filename: filename to check
    :param strip_sls: strip the .sls from a filename before adding it.
    :return the list after operation
    """
    if __salt__["pillar.file_exists"](filename):
        if strip_sls:
            filename = filename.replace(".sls", "")
        in_list.append(filename)
    return in_list


def run():
    """
    Renders the base environment pillar assignment.
    Assigns the minions their pillar file based on the name they have.
    """
    minion_id = __opts__["id"]

    # We use the fqdn as the default minion_id, so splitting on
    # the first . gets us the plain hostname
    hostname, domainname = minion_id.split(".", 1)
    if "-" in hostname:
        # We have multiple of the same type
        hostname, count = hostname.split("-", 1)

    # Replace the other . in domainnames with _
    # That makes filenames a lot easier
    domainname = domainname.replace(".", "_")

    relevant_pillar_files = []

    add_if_pillar_file_exists(relevant_pillar_files, "_from_terraform.sls")
    add_if_pillar_file_exists(relevant_pillar_files, "environments/defaults.sls")
    add_if_pillar_file_exists(relevant_pillar_files, f"environments/{domainname}.sls")
    add_if_pillar_file_exists(relevant_pillar_files, f"minions/defaults/{hostname}.sls")
    add_if_pillar_file_exists(relevant_pillar_files, f"minions/{domainname}/{hostname}.sls")

    pillar_top = {"base": {"*": relevant_pillar_files}}

    return pillar_top

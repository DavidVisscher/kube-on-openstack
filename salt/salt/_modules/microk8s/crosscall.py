"""
Sets up the requirement for crosscalling modules.
See:
https://docs.saltproject.io/en/latest/ref/clients/index.html#salt-s-loader-interface
"""

import salt.config
import salt.loader

__opts__ = salt.config.minion_config("/etc/salt/minion")
__grains__ = salt.loader.grains(__opts__)
__opts__["grains"] = __grains__
__utils__ = salt.loader.utils(__opts__)
__salt__ = salt.loader.minion_mods(__opts__, utils=__utils__)

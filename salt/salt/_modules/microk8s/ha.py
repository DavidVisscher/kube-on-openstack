"""
Module for managing microk8s HA connections.
"""
import time

import yaml

from .status import status
from .crosscall import __salt__, __opts__, __grains__

from .constants import MICROK8S_PATH


def ha_status():
    """
    Returns the current HA status for microk8s
    :return:
    """
    return status()["high-availability"]


def ha_enabled():
    """
    Returns whether HA is enabled
    :return:
    """
    return ha_status()["enabled"]


def ha_nodes():
    """
    Returns list of currently connected HA nodes
    :return:
    """
    return ha_status()["nodes"]


def is_joined_with(other_node_ip):
    """
    Checks if this current node is joined with another node.
    :param other_node_ip: the IP of the other node it should be joined with
    :return: bool
    """
    for node in ha_nodes():
        if other_node_ip in node["address"]:
            return True
    return False


def ha_issue_token(token=None, token_ttl=600):
    """
    Issues a token from this node that can be used to join it.
    :param token_ttl: Token time to life. 10 minutes by default.
    :param token: Token to issue, must be 32 characters.
    :return:
    """

    token_command = f"{MICROK8S_PATH} add-node --format yaml --token-ttl {token_ttl}"
    if token is not None:
        token_command += f" --token {token}"
    token_command_output = __salt__["cmd.run"](token_command)
    token_dict = yaml.load(token_command_output, Loader=yaml.SafeLoader)
    return token_dict


def ha_join(
    join_url,
    worker_only=False,
    env=None,
    wait_for_join=True,
    wait_timeout=300,
    wait_step=5,
):
    """
    Joins this node to another node.
    :param join_url: URL to connect to join the cluster.
    :param worker_only: join as a worker only, do not host control plane services
    :param env: additional environment variables when running command
    :param wait_for_join: wait until the node has joined the cluster
    :param wait_timeout: maximum time to wait in seconds
    :param wait_step: seconds between checking if joining has finished
    :return:
    """
    if env is None:
        env = {"LANG": "C.UTF-8", "LC_ALL": "C.UTF-8"}

    join_command = f"{MICROK8S_PATH} join {join_url}"
    if worker_only:
        join_command += " --worker"
    join_command_output = __salt__["cmd.run"](join_command, env=env)

    if wait_for_join:
        total_sleep_time = 0
        while (
            not is_joined_with(join_url.split(":")[0])
            and total_sleep_time < wait_timeout
        ):
            time.sleep(wait_step)
            total_sleep_time += wait_step

    return join_command_output

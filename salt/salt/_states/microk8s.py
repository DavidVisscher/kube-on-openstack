"""
State module for managing microk8s.
"""


def running(name):
    """
    Ensures that microk8s is running on a node.
    :param name: state name
    :param token: Authentication to use when adding a node to a cluster. Must be specified for HA.
    :return:
    """
    ret = {"name": name, "result": False, "changes": {}, "comment": ""}

    if not __salt__["microk8s.is_running"]():
        if __opts__["test"]:
            ret["result"] = None
            ret["comment"] = "Microk8s would be started"
            return ret

        result = __salt__["microk8s.start"]()
        if __salt__["microk8s.is_running"]():
            ret["comment"] = "Microk8s was started"
            ret["result"] = True
            ret["changes"] = {"start": result}
            return ret
        else:
            ret["comment"] = "Starting microk8s failed"

    ret["comment"] = "Microk8s is already running"
    ret["result"] = True
    return ret


def addon_enabled(name):
    """
    Ensures that a certain addon is enabled for microk8s
    :param name: the addon to enable
    :return:
    """
    ret = {"name": name, "result": False, "changes": {}, "comment": ""}

    if not __salt__["microk8s.addon_is_enabled"](name):
        if __opts__["test"]:
            ret["result"] = None
            ret["comment"] = f"Addon {name} would be enabled"
            return ret

        result = __salt__["microk8s.enable_addon"](name)
        if __salt__["microk8s.addon_is_enabled"](name):
            ret["comment"] = f"Addon {name} has been enabled"
            ret["result"] = True
        else:
            ret["comment"] = f"Enabling addon {name} failed"
            ret["result"] = False
        ret["changes"] = {"enable": result}
        return ret

    ret["comment"] = f"Addon {name} is already enabled"
    ret["result"] = True
    return ret


def addon_disabled(name):
    """
    Ensures that a certain addon is disabled for microk8s
    :param name: the addon to disable
    :return:
    """
    ret = {"name": name, "result": False, "changes": {}, "comment": ""}

    if __salt__["microk8s.addon_is_enabled"](name):
        if __opts__["test"]:
            ret["result"] = None
            ret["comment"] = f"Addon {name} would be disabled"
            return ret

        result = __salt__["microk8s.disable_addon"](name)

        if not __salt__["microk8s.addon_is_enabled"](name):
            ret["comment"] = f"Addon {name} has been disabled"
            ret["result"] = True
        else:
            ret["comment"] = f"Disabling addon {name} failed"
            ret["result"] = False
        ret["changes"] = {"disable": result}
        return ret

    ret["comment"] = f"Addon {name} is already disabled"
    ret["result"] = True
    return ret


def cluster_joined(name, worker_only=False, url_prefix_filter=""):
    """
    Ensures this node is joined with another microk8s master node
    :param url_prefix_filter: prefix to filter for when selecting the correct connection url
    :param master_node_id: salt minion_id of the node to join
    :param worker_only: do not join the control plane
    :return:
    """
    ret = {"name": name, "result": False, "changes": {}, "comment": ""}
    master_node_id = name

    ip_addrs = __salt__["mine.get"](master_node_id, "network.ip_addrs")
    master_addrs = ip_addrs[master_node_id]

    already_joined = False
    for addr in master_addrs:
        if __salt__["microk8s.is_joined_with"](addr):
            already_joined = True
            continue

    if already_joined:
        ret["comment"] = f"Node has already joined {master_node_id}"
        ret["result"] = True
        return ret

    # Get the token to use to connect to
    master_token_data = __salt__["mine.get"](master_node_id, "microk8s.ha_issue_token")
    for url in master_token_data[master_node_id]["urls"]:
        if url.startswith(url_prefix_filter):
            join_url = url
            continue

    # Handle the test case
    if __opts__["test"]:
        ret["result"] = None
        ret["comment"] = f"Node would have joined {master_node_id} at {join_url}"
        return ret

    # Actually start the join
    join_result = __salt__["microk8s.ha_join"](join_url, worker_only=worker_only)

    # Check if join was successful
    for addr in master_addrs:
        if __salt__["microk8s.is_joined_with"](addr):
            ret["result"] = True
            ret["comment"] = f"Node successfully joined microk8s at {master_node_id}"
            ret["changes"] = {"join": join_result}
            return ret

    # if not successful
    ret["result"] = False
    ret["comment"] = (
        f"An error occurred when attempting to join cluster at {master_node_id} "
        f"using {join_url}"
    )
    ret["changes"] = {"join": join_result}
    return ret

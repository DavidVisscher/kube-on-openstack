bastion_fail2ban_package:
  pkg.installed:
    - name: fail2ban

bastion_fail2ban_jail_conf:
  file.managed:
    - name: /etc/fail2ban/jail.d/customisation.local
    - contents: |
        [sshd]
        enabled = true

bastion_fail2ban_running:
  service.running:
    - name: fail2ban
    - enable: True
    - reload: True
    - watch:
        - file: bastion_fail2ban_jail_conf
    - require:
        - pkg: bastion_fail2ban_package

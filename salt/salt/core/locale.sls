c_locale:
  locale.present:
    - name: C.UTF-8

default_locale:
  locale.system:
    - name: C.UTF-8
    - require:
      - locale: c_locale
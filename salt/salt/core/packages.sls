packages_up_to_date:
  pkg.uptodate:
    - refresh: True

install_snapd:
  pkg.installed:
    - name: snapd

core_dependencies:
  pkg.installed:
    - pkgs:
      - python3-m2crypto
{% for user in pillar.get('users', []) %}
user_{{ user.name }}_present:
  user.present:
    - name: {{ user.name }}
    - shell: /bin/bash
    - createhome: True
    - optional_groups:
        - wheel
        - sudoers
        - sudo
        - admin
  ssh_auth.present:
    - name: {{ user.ssh_public_key }}
    - user: {{ user.name }}
    - comment: "{{ user.name }} via terraform"
{% endfor %}

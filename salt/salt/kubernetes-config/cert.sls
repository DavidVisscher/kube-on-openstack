{% set pki_dir_path = "/etc/pki/kubernetes" %}
{% set ca_cert_path = pki_dir_path+"/ca.crt" %}

kubernetes_cert_dir:
  file.directory:
    - name: {{ pki_dir_path }}
    - makedirs: True
    - clean: True

kubernetes_cert_get_ca:
  x509.pem_managed:
    - name: {{ ca_cert_path }}
    - text: {{ salt['mine.get']('ca', 'x509.get_pem_entries')['ca']['/etc/pki/kubernetes-ca/ca.crt'] | replace('\n', '') }}

kubernetes_cert_worker_key:
  x509.private_key_managed:
    - name: {{ pki_dir_path }}/worker.key
    - bits: 4096
    - backup: True

kubernetes_cert_worker_cert:
  x509.certificate_managed:
    - ca_server: ca
    - signing_policy: kubernetes
    - public_key: {{ pki_dir_path }}/worker.key
    - cn: {{ grains['id'] }}
    - days_remaining: 30
    - backup: True
microk8s_enable_dns:
  microk8s.addon_enabled:
    - name: dns

microk8s_enable_hostpath_storage:
  microk8s.addon_enabled:
    - name: hostpath-storage

microk8s_enable_helm3:
  microk8s.addon_enabled:
    - name: helm3

microk8s_enable_dashboard:
  microk8s.addon_enabled:
    - name: dashboard

microk8s_enable_ingress:
  microk8s.addon_enabled:
    - name: ingress

microk8s_enable_rbac:
  microk8s.addon_enabled:
    - name: rbac
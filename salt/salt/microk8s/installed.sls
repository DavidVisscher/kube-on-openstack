microk8s_snap:
  cmd.run:
    - name: "/usr/bin/snap install microk8s --classic --channel={{ salt['pillar.get']('microk8s:channel', 'latest/stable') }}"
    - creates:
        - "/snap/bin/microk8s"

{% if salt['pillar.get']("microk8s:proxy:enabled", False) %}
microk8s_containerd_proxy:
  file.keyvalue:
    - name: /var/snap/microk8s/current/args/containerd-env
    - separator: "="
    - count: 1
    - append_if_not_found: True
    - show_changes: True
    - require:
        - cmd: microk8s_snap
    - require_in:
        - microk8s: microk8s_started
    - key_values:
        HTTP_PROXY: "{{ salt['pillar.get']("microk8s:proxy:http") }}"
        HTTPS_PROXY: "{{ salt['pillar.get']("microk8s:proxy:https") }}"
        NO_PROXY: "{{ salt['pillar.get']("microk8s:proxy:no_proxy", "10.0.0.0/8,192.168.0.0/16,127.0.0.1,172.16.0.0/16") }}"
{% endif %}

microk8s_started:
  microk8s.running:
    - require:
        - cmd: microk8s_snap

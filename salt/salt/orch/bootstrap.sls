make_sure_all_up:
  salt.function:
    - name: test.ping
    - tgt: "*"

configure_salt_master:
  salt.state:
    - tgt: "salt*"
    - sls:
        - salt_master

ensure_sync:
  salt.function:
    - name: saltutil.sync_all
    - tgt : "*"

apply_states:
  salt.state:
    - tgt: "k8s*master*"
    - sls:
        - core
        - microk8s.installed

update_mine:
  salt.function:
    - name: mine.update
    - tgt: "*"

join_cluster:
  salt.state:
    - tgt: "k8s*"
    - sls:
        - microk8s.joined

enable_modules:
  salt.state:
    - tgt: "{{ salt["pillar.get"]("microk8s:master", "k8s-master-0") }}*"
    - sls:
        - microk8s.addons

final_highstate:
  salt.state:
    - tgt: "*"
    - highstate: True

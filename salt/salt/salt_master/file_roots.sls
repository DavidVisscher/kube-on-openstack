master_file_roots_config_file:
  file.managed:
    - name: /etc/salt/master.d/file_roots.conf
    - watch_in:
        - service: salt_master_service
    - contents: |
        file_roots:
          base:
            - /srv/salt
            - /srv/formulas/kubernetes-formula
          file_transfer:
            - /srv/file_transfer
base:
  'salt*':
    - core
    - salt_master
  'bastion*':
    - core
    - bastion
  'k8s*':
    - core
    - microk8s
  '*':
    - core

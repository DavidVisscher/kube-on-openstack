resource "openstack_networking_port_v2" "bastion_private" {
  name           = "bastion_private_port"
  network_id     = openstack_networking_network_v2.private.id
  admin_state_up = "true"

  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.private.id
    ip_address = "10.1.0.1"
  }

  allowed_address_pairs {
    # Allow sending of packets from other ips than its own.
    # This has to be specified to override Openstacks anti-spoofing system.
    ip_address = "0.0.0.0/0"
  }
}

resource "openstack_networking_port_v2" "bastion_public" {
  name           = "bastion_public_port"
  network_id     = openstack_networking_network_v2.public.id
  admin_state_up = "true"

  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.public.id
    ip_address = "10.0.0.10"
  }
}

resource "openstack_compute_instance_v2" "bastion" {
  name = "bastion.${var.deploy_domain_name}"
  security_groups = [
    "default",
    openstack_networking_secgroup_v2.ssh_all.name
  ]
  flavor_name = "m1.small"
  user_data = templatefile("${path.module}/cloud_init/bastion.tftpl", {
    public_ip         = openstack_networking_port_v2.bastion_public.fixed_ip[0].ip_address,
    private_ip        = openstack_networking_port_v2.bastion_private.fixed_ip[0].ip_address,
    deploy_public_key = openstack_compute_keypair_v2.deploy_key.public_key,
    set_root_password = var.set_root_password,
    root_password     = var.root_password,
    hostname          = "bastion",
    fqdn              = "bastion.${var.deploy_domain_name}",
    install_script    = filebase64("${path.module}/scripts/install_salt_minion.sh")
  })
  key_pair = openstack_compute_keypair_v2.deploy_key.id
  image_id = var.base_image

  network {
    port = openstack_networking_port_v2.bastion_public.id
  }

  network {
    port = openstack_networking_port_v2.bastion_private.id
  }

  connection {
    host        = openstack_networking_floatingip_associate_v2.bastion_float.floating_ip
    user        = var.deploy_user
    private_key = openstack_compute_keypair_v2.deploy_key.private_key
  }

  provisioner "remote-exec" {
    # Forces other resource creation to wait for bastion to become available.
    # This is needed so private-network instances have a way to connect outward.
    inline = [
      "cloud-init status --wait"
    ]
  }
}

resource "openstack_networking_floatingip_associate_v2" "bastion_float" {
  floating_ip = element(openstack_networking_floatingip_v2.reserved, 0).address
  port_id     = openstack_networking_port_v2.bastion_public.id
}

resource "null_resource" "bastion_install_salt_minion" {
  # Configures the salt minion on the bastion after the salt-master has been rolled out.
  # This is a separate resource to prevent chicken-egg problems.

  triggers = {
    # Trigger this when bastion is redeployed (and as such the ID changes)
    bastion_id = openstack_compute_instance_v2.bastion.id
  }

  depends_on = [
    openstack_compute_instance_v2.bastion,
    openstack_compute_instance_v2.salt
  ]

  connection {
    type        = "ssh"
    host        = openstack_networking_floatingip_associate_v2.bastion_float.floating_ip
    user        = var.deploy_user
    private_key = openstack_compute_keypair_v2.deploy_key.private_key
  }

  provisioner "remote-exec" {
    # Accept this minion's key
    # Attention! Executed on the salt-master

    connection {
      type = "ssh"
      user = var.deploy_user
      host = openstack_compute_instance_v2.salt.network[0].fixed_ip_v4

      private_key = openstack_compute_keypair_v2.deploy_key.private_key

      bastion_host        = openstack_networking_floatingip_associate_v2.bastion_float.floating_ip
      bastion_user        = var.deploy_user
      bastion_private_key = openstack_compute_keypair_v2.deploy_key.private_key
    }

    inline = [
      "sudo salt-key --yes --delete ${openstack_compute_instance_v2.bastion.name}" # In case we're replacing the instance
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "echo '${openstack_compute_instance_v2.salt.network[0].fixed_ip_v4} salt' | sudo tee -a /etc/hosts",
      "sudo bash /tmp/install_script.sh",
      "sudo systemctl restart salt-minion" # Forces a retransmit of keys if salt was already installed
    ]
  }

  provisioner "remote-exec" {
    # Accept this minion's key
    # Attention! Executed on the salt-master

    connection {
      type = "ssh"
      user = var.deploy_user
      host = openstack_compute_instance_v2.salt.network[0].fixed_ip_v4

      private_key = openstack_compute_keypair_v2.deploy_key.private_key

      bastion_host        = openstack_networking_floatingip_associate_v2.bastion_float.floating_ip
      bastion_user        = var.deploy_user
      bastion_private_key = openstack_compute_keypair_v2.deploy_key.private_key
    }

    inline = [
      "set +x",
      "sudo salt-key --yes --accept ${openstack_compute_instance_v2.bastion.name}",
      "sleep 10", # The salt minion needs a few seconds to come up after having its key accepted.
      "sudo salt ${openstack_compute_instance_v2.bastion.name} state.highstate"
    ]
  }
}

resource "null_resource" "cluster_bootstrap" {
  depends_on = [
    openstack_compute_instance_v2.salt,
    openstack_compute_instance_v2.k8s_master,
    openstack_compute_instance_v2.k8s_worker,
    null_resource.sync_salt_states
  ]

  connection {
    type = "ssh"
    user = var.deploy_user
    host = openstack_networking_port_v2.salt_private.fixed_ip[0].ip_address

    private_key = openstack_compute_keypair_v2.deploy_key.private_key

    bastion_host        = openstack_networking_floatingip_associate_v2.bastion_float.floating_ip
    bastion_user        = var.deploy_user
    bastion_private_key = openstack_compute_keypair_v2.deploy_key.private_key
  }

  provisioner "remote-exec" {
    inline = [
      "sudo salt-run state.orchestrate orch.bootstrap"
    ]
  }
}

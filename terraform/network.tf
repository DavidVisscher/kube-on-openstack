data "openstack_networking_network_v2" "external" {
  name = var.external_network_name
}

resource "openstack_networking_network_v2" "public" {
  name           = "kube_on_os_public_network"
  admin_state_up = "true"
  mtu            = 1450
}

resource "openstack_networking_subnet_v2" "public" {
  name       = "kube_on_os_public_subnet"
  network_id = openstack_networking_network_v2.public.id
  cidr       = "10.0.0.0/16"
  ip_version = 4
}

resource "openstack_networking_router_v2" "public" {
  name                = "kube_on_os_public_router"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.external.id
}

resource "openstack_networking_router_interface_v2" "public_subnet_interface" {
  router_id = openstack_networking_router_v2.public.id
  subnet_id = openstack_networking_subnet_v2.public.id
}

resource "openstack_networking_network_v2" "private" {
  name           = "kube_on_os_private_network"
  admin_state_up = "true"
  mtu            = 1450 
}

resource "openstack_networking_subnet_v2" "private" {
  name       = "kube_on_os_private_subnet"
  network_id = openstack_networking_network_v2.private.id
  cidr       = "10.1.0.0/16"
  ip_version = 4

  dns_nameservers = [
    "1.1.1.1",
    "1.0.0.1"
  ]
}

resource "openstack_networking_floatingip_v2" "reserved" {
  count = 2
  pool  = var.external_network_name

  lifecycle {
    prevent_destroy = false
  }
}

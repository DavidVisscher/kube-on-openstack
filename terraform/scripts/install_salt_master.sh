#!/bin/bash
set -e
set -x

curl -fsSL -o /usr/share/keyrings/salt-archive-keyring.gpg https://repo.saltproject.io/py3/ubuntu/20.04/amd64/3004/salt-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/salt-archive-keyring.gpg arch=amd64] https://repo.saltproject.io/py3/ubuntu/20.04/amd64/3004 focal main" | tee /etc/apt/sources.list.d/salt.list
apt-get update

export DEBIAN_FRONTEND=noninteractive # Solves prompt with file conflicts
apt-get install -y --force-yes -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" salt-master salt-minion

systemctl enable --now salt-master
systemctl enable --now salt-minion
sleep 5

/tmp/wait-for-it.sh -s -h localhost -p 4505 -t 300 -- echo "Done waiting for salt"

salt-key --yes --accept="salt*" # Accept own key

# Give the minion a second to open the connection
sleep 2
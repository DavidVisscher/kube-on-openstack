#!/bin/bash
set -e
set -x

curl -fsSL -o /usr/share/keyrings/salt-archive-keyring.gpg https://repo.saltproject.io/py3/ubuntu/20.04/amd64/3004/salt-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/salt-archive-keyring.gpg arch=amd64] https://repo.saltproject.io/py3/ubuntu/20.04/amd64/3004 focal main" | tee /etc/apt/sources.list.d/salt.list
apt-get update
apt-get install -y salt-minion

systemctl enable --now salt-minion
sleep 5
# DO NOT EDIT
# FILE CREATED BY TERRAFORM
# CHANGES WILL BE OVERWRITTEN

terraform_vars:
  deploy_domain: ${ deploy_domain }

users:
  - name: "${deploy_user}"
    ssh_public_key: "${deploy_public_key}"
%{ for user in users ~}
  - name: "${user.username}"
    ssh_public_key: "${user.ssh_public_key}"
%{ endfor ~}

microk8s:
  allowed_csr_ip: 194.171.203.19 

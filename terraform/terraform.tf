variable "num_kube_nodes" {
  type        = number
  description = "The amount of kubernetes nodes to deploy"
  default     = 3
}

variable "kube_node_flavor" {
  type        = string
  description = "The instance flavor to use when deploying kube nodes"
  default     = "m1.large"
}

variable "external_network_name" {
  type        = string
  description = "The name of the external network to use"
  default     = "vlan1066"
}

variable "base_image" {
  type        = string
  description = "The base image to use when creating instance volumes"
  default     = "ad156007-77bb-4e32-ac97-7f8340cab73d"
}

variable "deploy_domain_name" {
  type        = string
  description = "Domain to affix to instance names, used for env-specific variables."
  default     = "kube-on-os"
}

variable "deploy_user" {
  type        = string
  description = "Default user to use when deploying over ssh. Change when using a different OS."
  default     = "ubuntu"
}

variable "set_root_password" {
  type        = bool
  description = "Whether or not to set a root password on the machines, default: false"
  default     = false
}

variable "root_password" {
  type        = string
  description = "Root password to set on machines if set_root_password is true. FOR DEBUG ONLY!!!!!"
  sensitive   = true
}

variable "kubernetes_formula_url" {
  type        = string
  description = "URL to clone the kubernetes formula from"
  default     = "https://github.com/saltstack-formulas/kubernetes-formula.git"
}

variable "kubernetes_formula_version" {
  type        = string
  description = "Version of the salt kubernetes formula to use, from: https://github.com/saltstack-formulas/kubernetes-formula"
  default     = "v1.3.1"
}

variable "users" {
  type = list(object({
    username       = string
    ssh_public_key = string
  }))
  description = "Users to authorise for administrative tasks on the VM. Passed through to the salt users state."
  default     = []
}

terraform {
  backend "http" {
  }
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.49.0"
    }
    template = {
      source  = "hashicorp/template"
      version = "2.2.0"
    }
    archive = {
      source  = "hashicorp/archive"
      version = "2.2.0"
    }
  }
}

provider "openstack" {
  # This line below is unfortunately required
  # If left out, terraform will first try plain http,
  # and subsequently fail.
  auth_url = "https://merlin.hpc.rug.nl:5000/v3"
}
